public class Program {
    public static void main(String[] args) {
        Subject subject = new Subject();

        HexaObserver x1 = new HexaObserver(subject);
        OctalObserver x2 = new OctalObserver(subject);
        BinaryObserver x3 = new BinaryObserver(subject);

        System.out.println("Thử nghiệm 1 - Gán giá trị 15 cho subject");
        subject.setState(15);

        System.out.println("Thử nghiệm 2 - Gán giá trị 10 cho subject");
        subject.setState(10);

        System.out.println("Thử nghiệm 3 - Gỡ observer Binary");
        subject.remove(x3);
        subject.setState(150);
    }
}
