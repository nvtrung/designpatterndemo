public class OctalObserver extends Observer{

    public OctalObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Dạng bát phân: " + Integer.toOctalString( subject.getState() ) );
    }
}