﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverDemo.Entity
{
    class Sach
    {
        public String MaSach { get; set; }
        public String TenSach { get; set; }
        public int NamXuatBan { get; set; }
    }
}
