﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObserverDemo.Entity;

namespace ObserverDemo.DAL
{
    class FakeSachRepository : ISachRepository
    {
        public List<Sach> GetAllSach()
        {
            List<Sach> lst = new List<Sach>();
            var s1 = new Sach() { MaSach = "S1", TenSach = "Sách 1", NamXuatBan = 1999 };
            var s2 = new Sach() { MaSach = "S234", TenSach = "fsdfsdf", NamXuatBan = 1992 };
            var s3 = new Sach() { MaSach = "S2314", TenSach = "Sách 1124124", NamXuatBan = 2000 };
            var s4 = new Sach() { MaSach = "S31", TenSach = "Sách 131", NamXuatBan = 2019 };
            lst.Add(s1);
            lst.Add(s2);
            lst.Add(s3);
            lst.Add(s4);
            return lst;
        }
    }
}
