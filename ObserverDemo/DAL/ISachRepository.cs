﻿using ObserverDemo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverDemo.DAL
{
    interface ISachRepository
    {
        List<Sach> GetAllSach();
        // void insertSach(Sach x);
        // void deleteSach(Sach x);
        // void deleteSach(String maSach);
        //
    }
}
