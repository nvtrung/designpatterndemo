﻿using ObserverDemo.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObserverDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            napDanhSach();
        }

        void napDanhSach()
        {
            DAL.ISachRepository sachRepo = null;
            //sachRepo = new DAL.FakeSachRepository();
            sachRepo = Program.GetSachRepository();

            List<Sach> lst = sachRepo.GetAllSach();

            bsSach.DataSource = lst;
            bindingNavigator1.BindingSource = bsSach;


            lbxSach.DisplayMember = "TenSach";
            lbxSach.DataSource = bsSach;

            gridSach.DataSource = bsSach;
        }

        private void bsSach_CurrentChanged(object sender, EventArgs e)
        {
            // Cập nhật thông tin chi tiết của Sách đang chọn
            if (bsSach.Current != null)
            {
                var sach = bsSach.Current as Sach;
                txtMaSach.Text = sach.MaSach;
                txtTenSach.Text = sach.TenSach;
                txtNamXuatBan.Text = sach.NamXuatBan.ToString();
            }
            else
            {
                txtMaSach.Text = "";
                txtTenSach.Text = "";
                txtNamXuatBan.Text = "";
            }
        }

        private void btnNapdanhsach_Click(object sender, EventArgs e)
        {
            napDanhSach();
        }
    }
}
