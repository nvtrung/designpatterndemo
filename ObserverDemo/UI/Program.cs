﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObserverDemo
{
    static class Program
    {
        /// <summary>
        /// Áp dụng cơ chế REFLECTION
        /// </summary>
        /// <returns></returns>
        public static DAL.ISachRepository GetSachRepository()
        {
            string kiểuRepo = "";
            kiểuRepo = "đọc từ file cấu hình";
            kiểuRepo = "ObserverDemo.DAL.FakeSachRepository";

            var objectType = Type.GetType(kiểuRepo);
            var instantiatedObject = Activator.CreateInstance(objectType) as DAL.ISachRepository;
            return instantiatedObject;
        }
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
