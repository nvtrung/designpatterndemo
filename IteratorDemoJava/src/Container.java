public interface Container {
    /**
     * Trả về đối tượng điều khiển
     * cho phép [duyệt qua] danh sách
     * theo một cách thức nào đó
     * @return
     */
    public Iterator getIterator();
}
