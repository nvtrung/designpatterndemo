public interface Iterator {
    /**
     * Cho biết "danh sách" có còn phần tử nữa hay không
     * @return
     */
    public boolean hasNext();

    /**
     * Lấy phần tử ở [vị trí hiện tại]
     * @return
     */
    public Object next();
}
