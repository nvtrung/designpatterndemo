public class MonthRepository implements Container {
    public String months[] = {"Tháng Giêng", "Tháng Hai", "Tháng Ba", "Tháng Tư", "Tháng Năm",
            "Tháng Sáu", "Tháng Bảy", "Tháng Tám", "Tháng Chín", "Tháng Mười", "Tháng Mười một", "Tháng Chạp"};

    @Override
    public Iterator getIterator() {
        return new MonthIterator();
    }

    private class MonthIterator implements Iterator {
        private int viTriHienTai;

        @Override
        public boolean hasNext() {
            if (viTriHienTai < months.length) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return months[viTriHienTai++];
            }
            return null;
        }
    }
}