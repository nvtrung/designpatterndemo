package commands;

import ui.Editor;

public abstract class Command {
    public Editor editor;
    private String _nộiDungCũ;

    Command(Editor editor) {
        this.editor = editor;
    }

    void backup() {
        _nộiDungCũ = editor.textField.getText();
    }

    public void undo() {
        editor.textField.setText(_nộiDungCũ);
    }

    public abstract boolean execute();
}