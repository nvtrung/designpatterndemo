﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MementoDemo
{
    // The Concrete Memento contains the infrastructure for storing the Originator's state.
    class ConcreteMemento : IMemento
    {
        /// TODO: Sử dụng _state theo kiểu là một SerializationString của đối tượng
        private string _state;

        /// <summary>
        /// Ngày giờ tạo bản sao lưu
        /// </summary>
        private DateTime _date;

        public ConcreteMemento(string state)
        {
            this._state = state;
            this._date = DateTime.Now;
        }

        // The Originator uses this method when restoring its state.
        public string GetState()
        {
            return this._state;
        }

        // The rest of the methods are used by the Caretaker to display metadata.
        // Tên gợi nhớ của bản sao lưu
        public string GetName()
        {
            return $"{this._date} / ({this._state.Substring(0, 9)})...";
        }

        public DateTime GetDate()
        {
            return this._date;
        }
    }
}
