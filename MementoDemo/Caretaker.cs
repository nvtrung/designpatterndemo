﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MementoDemo
{
    // The Caretaker doesn't depend on the Concrete Memento class. Therefore, it
    // doesn't have access to the originator's state, stored inside the memento.
    // It works with all mementos via the base Memento interface.
    class Caretaker
    {
        // History của originator
        private List<IMemento> _mementos = new List<IMemento>();

        private Originator _originator = null;

        public Caretaker(Originator originator)
        {
            this._originator = originator;
        }

        public void Backup()
        {
            Console.WriteLine("\nCaretaker: Saving Originator's state...");
            this._mementos.Add(this._originator.SaveSnapshot());
        }

        public void Undo()
        {
            if (this._mementos.Count == 0)
            {
                return;
            }

            #region var snapshot = lấy snapshot cuối cùng trong danh mục history
            var snapshot = this._mementos.Last();
            this._mementos.Remove(snapshot);
            #endregion

            #region _originator <-- snapshot
            try
            {
                Console.WriteLine("Caretaker: Restoring state to: " + snapshot.GetName());
                this._originator.RestoreFromSnapshot(snapshot);
            }
            catch (Exception)
            {
                this.Undo();
            }
            #endregion
        }

        /// <summary>
        /// Hiển thị danh sách các hisotry entry đã được lưu trữ
        /// </summary>
        public void ShowHistory()
        {
            Console.WriteLine("Caretaker: Here's the list of mementos:");

            foreach (var memento in this._mementos)
            {
                Console.WriteLine(memento.GetName());
            }
        }
    }
}
